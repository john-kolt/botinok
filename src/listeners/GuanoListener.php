<?php
require_once dirname(__FILE__)."/../vk/VkBotListener.php";
require_once dirname(__FILE__)."/util/VkMessage.php";

class GuanoListener implements VkBotListener {
    protected $guanos = [];
    
    function __construct() {
        $this->guanos = [
            "/\bкок[е|и]{1}р\b/ui" => "кокер",
            "/\bформул[а|ы|е|у]{1}\b/ui" => "формула",
            "/\bкон[а|ы|е|у]{1}\b/ui" => "кона",
            "/\bш[у|y]т[е|e|и][р|p][а|ы|е|e|у|y]{0,1}\b/ui" => "шутер",
            "/\bреб[а|у]{1}\b/ui" => "реба",
            "/\bфр[и|е]ройд\b/ui" => "фреройд",
            "/\bшиман[а|у|о]{1}\b/ui" => "шимано",
            "/\bсрам[а|у]{0,1}\b/ui" => "срам",
            "/\b[к|г]{1}[о|а]{1}[м]{1,2}ен[ц|с]{1}аль\b/ui" => "коменсаль",
            "/\b(п[и|е]тон[а|у]{0,1}|python)\b/ui" => "питон",
            "/\b([д]{0,1}жав[а|у|е|ы]{1}|java)\b/ui" => "джава",
            "/\b([д]{0,1}жа[б|в]{1}аскрипт[а|у|е|ы]{0,1}|js|javascript)\b/ui" => "джаваскрипт",
            "/\b(норк[а|у|е|и|о]{1}|norco)\b/ui" => "норко",
            "/\b(лапьер[а|у|е|ы]{0,1}|lapierre)\b/ui" => "лапьер",
            "/\b(пуш[и|е]р[а|у|е|ы]{0,1}|pusher)\b/ui" => "пушер",
            "/\b([с|ш]тарк[а|у|е|и]{0,1}|stark)\b/ui" => "старк",
        ];
    }

    public function getEventType() {
        return 4; // new messages
    }
    
    public function execute(VkApi $api, Array $args) {
        $message = new VkMessage($args);

        $outbox = ($message->flags & 2) === 2;
        //if ($outbox) return; // do not process outgoing messages
        if (preg_match('/\b#(ботинок)\b/ui', $message->text)) return false;

        $stopPropagation = false;
        $data = ["говно", "пиздец", "хуйня", "уебанство", "ниочень", "чугуний", "пережитокпрошлого", "ахтунг", "хуита", "провал"];
        foreach ($this->guanos as $regexp => $name) {
            if (preg_match($regexp, $message->text, $args)) {
                $api->sendMessage($message->from_id, "#{$name}". $data[array_rand($data)]);
                $stopPropagation = true;
                break;
            }
        }
        return $stopPropagation;
    }
}
