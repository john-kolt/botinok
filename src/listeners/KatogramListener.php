<?php
require_once dirname(__FILE__)."/../vk/VkBotListener.php";
require_once dirname(__FILE__)."/util/VkMessage.php";

class KatogramListener implements VkBotListener {
	
    public function getEventType() {
		return 4; // new messages
	}
	
    public function execute(VkApi $api, Array $args) {
		$message = new VkMessage($args);
		
        $outbox = ($message->flags & 2) === 2;
        //if ($outbox) return; // do not process outgoing messages
        if (preg_match('/#(ботинок)\b/ui', $message->text)) return false;

        $regexps = [
            "/\b(регламент[а|у|ы]{0,1}})\b/ui" => function ($args) use ($api, $message) {
                $api->sendMessage($message->from_id, "#катограм #регламент<br>1. Никому не говорить о ночной катке<br>"
													."2. Никому и никогда не говорить о ночной катке<br>"
													."3. Каждый катает в свою силу, при дальней поездке периодически ждем ВСЕХ отставших<br>"
													."4. На катку приезжаем сытыми, поездка в ленту идеальна в 3-4 часа утра<br>"
													."5. Утренний кофе не обсуждается<br>"
													."6. До утра доживут избранные");
            },
            "/^(?=.*\b(когда)\b)(?=.*\b(рам[а,у]|кукуруз|сантакруз|санта круз|номад)\b)(?=.*\b(привезут|будет|приедет)\b).+/ui" => function ($args) use ($api, $message) {
                $api->sendMessage($message->from_id, "8 мая доставлена в транспортную компанию, в Ижевске будет примерно в 11 мая<br><br>#santacruzbicycles");
            }
        ];

		$stopPropagation = false;
        foreach ($regexps as $regexp => $func) {
            if (preg_match($regexp, $message->text, $args)) {
                $func($args);
				$stopPropagation = true;
                break;
            }
        }
		return $stopPropagation;
	}
}