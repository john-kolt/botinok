<?php 

class VkMessage {
	public $message_id;
	public $flags;
	public $from_id;
	public $timestamp;
	public $subject;
	public $text;
	public $attachments;
	
	function __construct($args) {
		$this->message_id = $args[1];
		$this->flags = $args[2];
		$this->from_id = $args[3];
		$this->timestamp = $args[4];
		$this->subject = $args[5];
		$this->text = $args[6];
		$this->attachments = $args[7];
	}
}